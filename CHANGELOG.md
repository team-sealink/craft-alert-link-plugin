# Craft Alert Link Plugin - Changelog

## Unreleased

* [PLAT-483] Add decommissioned status

## 1.0.0

* [DC-2949] Correct plugin version and add branch-alias
* [DC-2899] Create the plugin
