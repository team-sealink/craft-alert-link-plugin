# README #

# This project is now decommissioned

### Introduction ###

* This is a craft plugin that is made for One Sealink website to visit Alert Link API
* The plugin does not create HTML contents for displaying alerts
* The plugin checks `.alertlink` and fill alert value into `.alert-value`
* When the Alert Link response is not `200`, `.alertlink` will be hidden, and `.alertlink-error` will be shown
* One Sealink should use this as a git submodule

### Test ###

* To run test: `bin/semaphore/test`
* The plugin requires `"guzzle/guzzle": "3.7"` for testing. It is a depreciated version, but it is used in One Sealink (Craft 2)

### Usage ###

* Install the plugin in One Sealink
* Configure Alert Link API `url`, `api_key`, etc.
* Add customized HTML in the required page where alert link is needed (example below)
* Enable the plugin

### An example of HTML content in page ###

```html
<div class="alertlink hide">
  <div>Current queue length: <strong class="alert-value"><em class="fa fa-spinner fa-pulse fa-fw"></em></strong> minutes.</div>
</div>
<div class="alertlink-error error hide">
  Sorry, we are unable to fetch queue length at the moment.
</div>
```
