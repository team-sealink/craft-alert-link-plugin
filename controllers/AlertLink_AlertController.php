<?php
namespace Craft;
use \Guzzle\Http\Client;

class AlertLink_AlertController extends BaseController
{
  protected $allowAnonymous = true;

  public function actionGetAlert() {
    $this->requireAjaxRequest();

    $url = craft()->alertLink->getAlertLinkSettings()['url'];
    $auth_token = craft()->alertLink->getAlertLinkSettings()['alertLinkApiKey'];
    $options = ['headers' =>
      [
        'Authorization' => "Bearer {$auth_token}"
      ]
    ];

    $guzzle = new Client();
    $response = craft()->alertLink->sendGet($url, $options, $guzzle);

    $this->returnJson($response);
  }

  public function actionGetSettings() {
    $this->requireAjaxRequest();

    $settings = craft()->alertLink->getAlertLinkSettings();
    $this->returnJson($settings);
  }
}
