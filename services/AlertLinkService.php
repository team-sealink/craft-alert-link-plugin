<?php namespace Craft;
use \Guzzle\Http\Exception\BadResponseException;

class AlertLinkService extends BaseApplicationComponent {

  public $plugin = null;
  public $settings = array();

  public function __construct()
  {
    $this->plugin   = craft()->plugins->getPlugin('alertlink');
    $this->settings = $this->plugin->getSettings();
  }

  public function isEnabled() {
    return $this->settings->enabled == '1';
  }

  public function getAlertLinkSettings() {
    $url = "{$this->settings->alertLinkUrl}/{$this->settings->alertTopic}/alerts/{$this->settings->alertName}";
    return [
      'enabled' => $this->isEnabled(),
      'alertLinkApiKey' => $this->settings->alertLinkApiKey,
      'url' => $url
    ];
  }

  public function sendGet($url, $options, $guzzle) {
    // Craft doesn't really expose a way to send raw text without
    // digging into the Yii framework plumbing so decoding the
    // response before we send it on

    try {
      $response = $guzzle->get($url, [], $options)->send();
      $body = $response->getBody();
    } catch (BadResponseException $e) {
      $body = $e->getResponse()->getBody(true);
    }

    return json_decode($body, true);
  }
}
