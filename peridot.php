<?php
use Evenement\EventEmitterInterface;
use \Mockery as mockery;
use Peridot\Reporter\CodeCoverage\CodeCoverageReporter;
use Peridot\Reporter\CodeCoverageReporters;

return function(EventEmitterInterface $emitter) {
  $coverage = new CodeCoverageReporters($emitter);
  $coverage->register();
  $emitter->on('code-coverage.start', function (CodeCoverageReporter $reporter) {
    $reporter->addDirectoryToWhiteList(__DIR__.'/services');
  });

  mockery::mock('alias:Craft\BaseApplicationComponent');
  $config   = mockery::mock('Craft\ConfigService');
  $settings	= mockery::mock('alias:Craft\Model');
  $plugin   = mockery::mock();
  $plugins  = mockery::mock();

  $plugin->shouldReceive('getSettings')->andReturn($settings);
  $plugins->shouldReceive('getPlugin')->with(mockery::any())->andReturn($plugin);
  $config->plugins = $plugins;

  $GLOBALS['config'] = $config;

  function craft() { return $GLOBALS['config']; }

  /*
	$config->shouldReceive('getIsInitialized')->andReturn(true);
	$config->shouldReceive('usePathInfo')->andReturn(true)->byDefault();

	$config->shouldReceive('get')->with('usePathInfo')->andReturn(true)->byDefault();
	$config->shouldReceive('get')->with('cpTrigger')->andReturn('admin')->byDefault();
	$config->shouldReceive('get')->with('pageTrigger')->andReturn('p')->byDefault();
	$config->shouldReceive('get')->with('actionTrigger')->andReturn('action')->byDefault();
	$config->shouldReceive('get')->with('translationDebugOutput')->andReturn(false)->byDefault();

	$config->shouldReceive('getLocalized')->with('loginPath')->andReturn('login')->byDefault();
	$config->shouldReceive('getLocalized')->with('logoutPath')->andReturn('logout')->byDefault();
	$config->shouldReceive('getLocalized')->with('setPasswordPath')->andReturn('setpassword')->byDefault();

	$config->shouldReceive('getCpLoginPath')->andReturn('login')->byDefault();
	$config->shouldReceive('getCpLogoutPath')->andReturn('logout')->byDefault();
	$config->shouldReceive('getCpSetPasswordPath')->andReturn('setpassword')->byDefault();
	$config->shouldReceive('getResourceTrigger')->andReturn('resource')->byDefault();
  */

	//$_SERVER['REMOTE_ADDR']	= '127.0.0.1';
};
