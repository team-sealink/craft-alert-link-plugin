<?php namespace Craft;

class AlertLinkPlugin extends BasePlugin
{
  function getName()
  {
    return Craft::t('Alert Link Plugin');
  }

  function getVersion()
  {
    return '1.0.0';
  }

  function getDeveloper()
  {
    return 'SeaLink Travel Group';
  }

  function getDeveloperUrl()
  {
    return 'https://sealinktravelgroup.com.au';
  }

  protected function defineSettings()
  {
    return array(
      'enabled'               => array(AttributeType::Bool, 'default' => false),
      'alertLinkUrl'          => AttributeType::String,
      'alertLinkApiKey'       => AttributeType::String,
      'alertName'             => AttributeType::String,
      'alertTopic'            => AttributeType::String
    );
  }

  function getSettingsHtml()
  {
    $settings = $this->getSettings();
    return craft()->templates->render('alertlink/settings', array(
      'settings' => $settings,
    ));
  }

  function definesResources()
  {
    return !empty($this->getJsResourcePaths()) || !empty($this->getCssResourcePaths());
  }

  function getJsResourcePaths() {
    return ['alertlink.js'];
  }

  public function getCssResourcePaths() {
    return [];
  }
}