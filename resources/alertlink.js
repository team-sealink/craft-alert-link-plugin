var AlertLink = AlertLink || {};

AlertLink.loadAlertLink = function() {
  $('.alertlink').removeClass('hide');
  $.ajax({
    url: '/actions/alertLink/alert/getAlert',
    success: function(response, textStatus, jqXHR) {
      if (response && response.alert_value) {
        $('.alertlink').find('.alert-value').html(response.alert_value);
      } else {
        AlertLink.showError(response);
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {
      AlertLink.showError(errorThrown);
    }
  });
}

AlertLink.showError = function(response) {
  console.log(response);
  $('.alertlink').addClass('hide');
  $('.alertlink-error').removeClass('hide');
}

AlertLink.checkSettings = function() {
  $.ajax({
    url: '/actions/alertLink/alert/getSettings',
    success: function(response, textStatus, jqXHR) {
      if (response.enabled) {
        AlertLink.loadAlertLink();
      }
    }
  });
}

$(document).ready(function() {
  if ($('.alertlink').length > 0) {
    AlertLink.checkSettings();
  }
});
