<?php namespace Craft;

class AlertlinkVariable
{

  protected $plugin = null;
  protected $settings = array();

  private $service = null;

  public function __construct()
  {
    $this->plugin   = craft()->plugins->getPlugin('alertlink');
    $this->settings = $this->plugin->getSettings();
    $this->service  = new AlertLinkService();
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->plugin->getName();
  }

  /**
   * @return string
   */
  public function getVersion()
  {
    return $this->plugin->getVersion();
  }

  /**
   * @return boolean
   */
  public function isEnabled() {
    return $this->service->isEnabled();
  }
}
