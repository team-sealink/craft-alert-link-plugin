<?php
require_once __DIR__ . '/../services/AlertLinkService.php';
$service = new Craft\AlertLinkService();

describe('AlertLinkService', function() use ($service) {
  it('should initialize properly', function() {
    //constructor has to be run within an it block to register for code coverage
    $obj = new Craft\AlertLinkService();
    expect($obj->settings)->to->not->be->null;
  });

  beforeEach(function () use ($service) {
    $service->settings->enabled = '1';
    $service->settings->alertLinkUrl = 'https://alertlink.com.au';
    $service->settings->alertName = 'queue_length';
    $service->settings->alertLinkApiKey = 'testkey';
    $service->settings->alertTopic = 'bruny';
  });

  describe('getAlertLinkSettings', function() use ($service) {
    it('should return settings', function() use ($service) {
      $settings = $service->getAlertLinkSettings();
      expect($settings)->to->include->keys(['enabled','url','alertLinkApiKey']);
      expect($settings['url'])->to->equal("https://alertlink.com.au/bruny/alerts/queue_length");
      expect($settings['alertLinkApiKey'])->to->equal("testkey");
      expect($settings['enabled'])->to->be->a('boolean');
    });
  });

  describe('sendGet', function() use ($service) {
    it('should return decoded json', function() use ($service) {
      $plugin = new Guzzle\Plugin\Mock\MockPlugin();
      $plugin->addResponse(new Guzzle\Http\Message\Response(200, null, '{"alert_value":"10","alert_name":"test","topic":"bruny"}'));
      $client = new Guzzle\Http\Client();
      $client->addSubscriber($plugin);
      $response = $service->sendGet('test-url', [], $client);

      expect($response)->to->be->an('array');
      expect($response)->to->include->keys(['alert_value', 'alert_name', 'topic']);
      expect($response["alert_value"])->to->equal('10');
    });

    it('should not raise', function() use ($service) {
      $plugin = new Guzzle\Plugin\Mock\MockPlugin();
      $response = new Guzzle\Http\Message\Response(500);
      $plugin->addResponse(new Guzzle\Http\Message\Response(500, null, '{"message":"test error"}'));
      $client = new Guzzle\Http\Client();
      $client->addSubscriber($plugin);
      $response = $service->sendGet('error-url', [], $client);

      expect($response["message"])->to->equal('test error');
    });
  });
});
